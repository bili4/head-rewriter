"use strict";

let uniroot;
if (window.browser) {
  uniroot = browser;
} else if (window.chrome) {
  uniroot = chrome
}
let originElm = document.getElementById('origin');
let refererElm = document.getElementById('referer');
let cookieElm = document.getElementById('cookie');

let backgroundPage = uniroot.extension.getBackgroundPage();

originElm.value = backgroundPage.localStorage.getItem('origin') || 'https://www.bilibili.com';
refererElm.value = backgroundPage.localStorage.getItem('referer') || 'https://www.bilibili.com';
cookieElm.value = backgroundPage.localStorage.getItem('cookie') || '';

document.getElementById('save').addEventListener("click", (e) => {
 
  backgroundPage.localStorage.setItem('origin', originElm.value);
  backgroundPage.localStorage.setItem('referer', refererElm.value);
  backgroundPage.localStorage.setItem('cookie', cookieElm.value);
  backgroundPage.initHeaders();
});
