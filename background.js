"use strict";

let targetPages = ["https://api.vc.bilibili.com/*", 'https://api.bilibili.com/*', 'http://*.bilivideo.com/*', 'https://*.bilivideo.com/*', 'https://s.search.bilibili.com/*'];
let targetDomain = [/http:\/\/local[0-9]*\.bilibili\.com\/?/, /https:\/\/[^.]*\.pages\.dev\/?/]

let gheaders = Object.create(null);

function initHeaders() {
  gheaders['origin'] = localStorage.getItem('origin') || 'https://www.bilibili.com';
  gheaders['referer'] = localStorage.getItem('referer') || 'https://www.bilibili.com';
  gheaders['cookie'] = localStorage.getItem('cookie') || '';
}

function rewriteHeader(e) {
  if (targetDomain.findIndex(d => d.test(e.initiator)) == -1) return {}
  if (!e.requestHeaders) return {}
  let hasCookie = false;
  for (let v of e.requestHeaders) {
    let name = v.name.toLowerCase();
    switch (name) {
      case "x-cookie":
        hasCookie = true;
      case "x-origin":
      case "x-referer":
        v.name = name.substr(2)
        gheaders[v.name] = v.value;
        break;
      case 'origin':
      case 'referer':
        v.value = gheaders[name];
        break;
      case 'cookie':
        hasCookie = true;
        break;
      case 'access-control-allow-headers':
        v.value += ', X-CustomHeader';
        break;
    }
    if(!hasCookie) {
        e.requestHeaders.push({name: 'cookie', value: gheaders['cookie']});
    }
  }
  return { requestHeaders: e.requestHeaders };
}


initHeaders();

let uniroot;
if (window.browser) {
  uniroot = browser;
} else if (window.chrome) {
  uniroot = chrome
}
uniroot.webRequest.onBeforeSendHeaders.addListener(rewriteHeader,
  { urls: targetPages },
  ["blocking", "requestHeaders"]);
uniroot.webRequest.onBeforeSendHeaders.addListener(rewriteHeader,
  { urls: targetPages },
  ["blocking", "requestHeaders", "extraHeaders"]);

// uniroot.webRequest.onSendHeaders.addListener((e) => {
//   console.log(e)
// },
//   { urls: targetPages },
//   ["requestHeaders", "extraHeaders"]);

function listener(response) {
  if (targetDomain.findIndex(d => d.test(response.initiator)) == -1) return {}

  if (!response.responseHeaders) return {}

  for (let i = 0; i < response.responseHeaders.length; i++) {
    let name = response.responseHeaders[i].name.toLowerCase();
    switch (name) {
      case 'access-control-allow-origin':
        response.responseHeaders[i].value = response.initiator;
        break;
      case 'access-control-allow-headers':
        response.responseHeaders[i].value += ',x-cookie,x-origin,x-referer';
    }
  }
  return {
    responseHeaders: response.responseHeaders
  };
}

uniroot.webRequest.onHeadersReceived.addListener(
  listener,
  { urls: targetPages },
  ["blocking", "responseHeaders"]
);
uniroot.webRequest.onHeadersReceived.addListener(
  listener,
  { urls: targetPages },
  ["blocking", "responseHeaders", "extraHeaders"]
);
